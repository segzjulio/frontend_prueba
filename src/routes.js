// import 'dotenv/config'

const API_URL = process.env.VUE_APP_API_URL;
const ROUTES = {
    rut: {
        save: `${API_URL}/rut/save`,
        getAll: `${API_URL}/rut/getAll`,
        delete: (id) => `${API_URL}/rut/delete/${id}`,
        updated: (id) => `${API_URL}/rut/update/${id}`,
        get: (id) => `${API_URL}/rut/get/${id}`,
    }
}

export  {
    ROUTES
}