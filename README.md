************************************************************************************************************
*  Pasos a seguir para instalar el proyecto:                                                               *
*      1.-Clonar este repositorio del proyecto, de preferencia en el directorio C:/laragon/www/.           *
*      2.-Ejecutar el comando "npm install"                                                                *
*      3.-configurar el .env segun el dominio en el que tenga el proyecto backend (Existen 2 ejemplos ahí) *
*      4.-Ejecutar comando "npm run serve"                                                                 *
************************************************************************************************************